%%
%% This is file `garneaudoc.cls',
%%
%% Copyright (C) 2020 Vincent Goulet
%%
%% This file may be distributed and/or modified under the conditions
%% of the LaTeX Project Public License, either version 1.3c of this
%% license or (at your option) any later version. The latest version
%% of this license is in:
%%
%%    http://www.latex-project.org/lppl.txt
%%
%% and version 1.3c or later is part of all distributions of LaTeX
%% version 2006/05/20 or later.
%%
%% This work has the LPPL maintenance status `maintained'.
%%
%% The Current Maintainer of this work is Vincent Goulet
%% <vincent.goulet@act.ulaval.ca>.
%%
\NeedsTeXFormat{LaTeX2e}[2009/09/24]
\ProvidesClass{garneaudoc}%
  [2020/03/05 v0.1 Cegep Garneau essay class]
\RequirePackage{ifxetex}
\newif\ifCG@babel        \CG@babeltrue         % charger babel?
\newif\ifCG@hyperref     \CG@hyperreftrue      % charger hyperref?
\newif\ifCG@hassubtitle  \CG@hassubtitlefalse  % document a un sous-titre?
\newif\ifCG@haswordcount \CG@haswordcountfalse % nombre de mots sur page titre?
\DeclareOption{nobabel}{\CG@babelfalse}
\DeclareOption{nohyperref}{\CG@hyperreffalse}
\newcommand*{\CG@ptsize}{}
\DeclareOption{10pt}{%
  \PassOptionsToClass{10pt}{memoir}
  \renewcommand*{\CG@ptsize}{10}}
\DeclareOption{11pt}{%
  \PassOptionsToClass{11pt}{memoir}
  \renewcommand*{\CG@ptsize}{11}}
\DeclareOption{12pt}{%
  \PassOptionsToClass{12pt}{memoir}
  \renewcommand*{\CG@ptsize}{12}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{memoir}}
\ExecuteOptions{11pt,letterpaper}
\ProcessOptions\relax
\LoadClass[oneside]{memoir}
\ifxetex
  \RequirePackage{fontspec}
  \defaultfontfeatures{Ligatures=TeX}
\else
  \RequirePackage[T1]{fontenc}
\fi
\ifCG@babel
  \RequirePackage{babel}
  \RequirePackage[autolanguage]{numprint}
\fi
\ifCG@hyperref
  \RequirePackage{etoolbox}
  \AtEndPreamble{\RequirePackage{hyperref}}
\else
  \AtBeginDocument{%
    \@ifpackageloaded{hyperref}{\relax}{%
      \ClassError{garneaudoc}{%
        Package hyperref is required}
      {Delete option nohyperref or load the package in the preamble.}}}
\fi
\PassOptionsToPackage{%
  colorlinks,%
  urlcolor=CGurlcolor,%
  linkcolor=CGlinkcolor,%
  citecolor=CGcitecolor}{hyperref}
\RequirePackage{graphicx}
\RequirePackage{xcolor}
\RequirePackage{textcomp}
\AtBeginDocument{%
  \@ifpackageloaded{geometry}{%
    \ClassError{garneaudoc}{%
      Package geometry is incompatible with this class}
    {Use the memoir class facilities to change the page layout.}}{\relax}}
\definecolor{CGlinkcolor}{rgb}{0,0.4,0.6}        % liens internes
\definecolor{CGurlcolor}{rgb}{0.6,0,0}           % liens externes
\definecolor{CGcitecolor}{rgb}{0,0.5,0}      % citations
\setlrmarginsandblock{40mm}{30mm}{*}
\setulmarginsandblock{40mm}{30mm}{*}
\setheaderspaces{25mm}{*}{*}
\checkandfixthelayout[nearest]
\renewcommand{\@pnumwidth}{3em}
\renewcommand{\@tocrmarg}{4em}
\OnehalfSpacing
% \setlength{\parskip}{0.5\baselineskip}
\renewcommand{\tocheadstart}{\SingleSpacing\chapterheadstart}
\renewcommand{\lotheadstart}{\SingleSpacing\chapterheadstart}
\renewcommand{\lofheadstart}{\SingleSpacing\chapterheadstart}
\pagestyle{simple}
\newcommand*{\CG@phvfamily}{\fontencoding{T1}\fontfamily{phv}\selectfont}
\ifnum\CG@ptsize=10\relax
  \newcommand*{\CG@fonttitle}{\normalfont\huge\bfseries\CG@phvfamily}
  \newcommand*{\CG@fontsubtitle}{\normalfont\LARGE\bfseries\CG@phvfamily}
  \newcommand*{\CG@fontauthor}{\normalfont\Large\bfseries\CG@phvfamily}
  \newcommand*{\CG@fontprogram}{\CG@fontauthor}
  \newcommand*{\CG@fontbase}{\normalfont\Large\CG@phvfamily}
\fi
\ifnum\CG@ptsize=11\relax
  \newcommand*{\CG@fonttitle}{\normalfont\LARGE\bfseries\CG@phvfamily}
  \newcommand*{\CG@fontsubtitle}{\normalfont\Large\bfseries\CG@phvfamily}
  \newcommand*{\CG@fontauthor}{\normalfont\large\bfseries\CG@phvfamily}
  \newcommand*{\CG@fontprogram}{\CG@fontauthor}
  \newcommand*{\CG@fontbase}{\normalfont\large\CG@phvfamily}
\fi
\ifnum\CG@ptsize=12\relax
  \newcommand*{\CG@fonttitle}{\normalfont\Large\bfseries\CG@phvfamily}
  \newcommand*{\CG@fontsubtitle}{\normalfont\large\bfseries\CG@phvfamily}
  \newcommand*{\CG@fontauthor}{\normalfont\normalsize\bfseries\CG@phvfamily}
  \newcommand*{\CG@fontprogram}{\CG@fontauthor}
  \newcommand*{\CG@fontbase}{\normalfont\normalsize\CG@phvfamily}
\fi
\newcommand{\CG@maintitle}{}
\newcommand{\CG@subtitle}{}
\newcommand*{\CG@author}{}
\newcommand*{\CG@coursetitle}{}
\newcommand*{\CG@coursenumber}{}
\newcommand*{\CG@profname}{}
\newcommand*{\CG@department}{}
\newcommand{\titre}[1]{\renewcommand{\CG@maintitle}{#1}}
\newcommand{\soustitre}[1]{%
  \CG@hassubtitletrue
  \renewcommand{\CG@subtitle}{#1}}
\newcommand*{\auteur}[1]{\renewcommand*{\CG@author}{#1}}
\newcommand*{\titrecours}[1]{\renewcommand*{\CG@coursetitle}{#1}}
\newcommand*{\numerocours}[1]{\renewcommand*{\CG@coursenumber}{#1}}
\newcommand*{\nomprof}[1]{\renewcommand*{\CG@profname}{#1}}
\newcommand*{\departement}[1]{\renewcommand*{\CG@department}{#1}}
\newsavebox{\CG@wordcountbox}
\newlength{\CG@wordcountboxht}
\newcommand*{\mots}[1]{%
  \CG@haswordcounttrue
  \setbox\CG@wordcountbox=\hbox{\normalfont\CG@phvfamily Nombre de mots: #1}
  \setlength{\CG@wordcountboxht}{\ht\CG@wordcountbox}}
\newcommand{\pagetitre}{{%
    \clearpage
    \pagestyle{empty}
    \normalfont\CG@phvfamily
    \setlength{\parskip}{0pt}
    \centering
    \vspace*{-20mm}
    \ifCG@haswordcount
      \hfill\box\CG@wordcountbox
    \else
      \rule{0mm}{\CG@wordcountboxht}
    \fi
    \par
    \vspace*{34.5mm}
    \CG@author\par
    \CG@coursetitle\par
    \CG@coursenumber\par
    \vfill
    \CG@maintitle\par
    \ifCG@hassubtitle
      \CG@subtitle\par
    \fi
    \vfill
    Travail présenté à\par
    \CG@profname\par
    \vfill
    \CG@department\par
    Cégep Garneau\par
    \thedate\par
    \clearpage}}
\ifCG@babel
  \addto\captionsfrench{\renewcommand{\listfigurename}{Liste des figures}}
\fi
\renewenvironment{quote}{%
  \list{}{\rightmargin 10mm \leftmargin 10mm}%
  \item[]}{\endlist}
\renewenvironment{quotation}{%
  \list{}{%
    \SingleSpacing
    \listparindent 0em
    \itemindent    \listparindent
    \leftmargin    10mm
    \rightmargin   \leftmargin
    \parsep        6\p@ \@plus\p@}%
  \item[]}{\endlist}
\setsecnumdepth{subsection}
\endinput
%%
%% End of file `garneaudoc.cls'.
